package task1;

import task1.view.View;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}

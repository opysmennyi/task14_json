package task1.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1.model.base.Candy;
import task1.model.comparator.Comparator;

import java.util.List;

public class Controller {
    private static Logger LOG = LogManager.getLogger(Controller.class);

    public void execute();

     void printList(List<Candy> candies) {
        candies.sort(new Comparator());
        for (Candy candy : candies) {
            LOG.info(candy);
        }
    }
}

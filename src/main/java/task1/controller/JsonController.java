package task1.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1.controller.Controller;
import task1.model.parserjson.JsonParser;
import task1.model.base.Candy;
import task1.model.base.Candy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static task1.constants.Constants.LINK;


public class JsonController  implements Controller {

    private static Logger LOG = LogManager.getLogger(JsonController.class);
    @Override
    public void execute() {
        try {
            List<Candy> candies = JsonParser.getCandyList(LINK);
            printList(candies);
            LOG.info("Write to the json file:");
            GSONParser.writeCandyToJson(candies, LINK);
            LOG.info("Ok");
        } catch (FileNotFoundException e) {
            LOG.error("File Not Found Exception");
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error("IO Exception");
            e.printStackTrace();
        }
    }
}
package task1.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1.controller.Controller;
import task1.controller.JsonController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class View {

    private Logger LOG = LogManager.getLogger(View.class);
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;
    private BufferedReader reader;

    public View() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        List<Controller> controllers = new ArrayList<>();
        controllers.add(new JsonController());
        setMenu();
        setMethodsMenu(controllers);
    }

    private void setMethodsMenu(List<Controller> controllers) {
        methodsMenu = new LinkedHashMap<>();
        for (int i = 0; i < controllers.size(); i++) {
            methodsMenu.put(String.valueOf(i + 1), controllers.get(i));
        }
    }

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", "Run JSon");

    }

    public void outputMenu() {
        LOG.info("MENU: ");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                LOG.info(key + " - " + menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu = "Q";
        do {
            outputMenu();
            LOG.info("Select menu point.");
            try {
                keyMenu = reader.readLine().toUpperCase();
                methodsMenu.get(keyMenu).execute();
            } catch (IOException e) {
                LOG.error("IO Exception");
                e.printStackTrace();
            }
        } while (!keyMenu.equals('Q'));
    }
}

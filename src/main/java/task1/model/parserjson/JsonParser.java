package task1.model.parserjson;

import task1.model.base.Candy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
public class JsonParser {
    private static final Type CHECK = new TypeToken<List<Candy>>() {
    }.getType();

    public static List<Candy> getCandyList(String json) throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(json));
        return gson.fromJson(reader, CHECK);
    }

    public static void writeCandyToJson(List<Candy> candies, String path) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(candies);
        FileWriter writer = new FileWriter(path);
        writer.write(json);
        writer.close();
    }
}

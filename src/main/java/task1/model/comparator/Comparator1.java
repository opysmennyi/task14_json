package task1.model.comparator;

import task1.model.base.Candy;

public interface Comparator1 {
    int compare(Candy o1, Candy o2);
}

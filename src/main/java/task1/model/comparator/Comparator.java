package task1.model.comparator;

import task1.model.base.Candy;
import java.util.Comparator;

public class Comparator implements Comparator <Candy> {

        @Override
        public int compare(Candy o1, Candy o2) {
            return Double.compare(o1.getEnergy(), o2.getEnergy());
        }
    }


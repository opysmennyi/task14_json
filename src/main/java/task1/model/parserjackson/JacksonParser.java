package task1.model.parserjackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import task1.model.base.Candy;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    public static List<Candy> getCandyList(File json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Candy[] candies = mapper.readValue(json, Candy[].class);
        return Arrays.asList(candies);
    }

    public static void writeCandyToJson(List<Candy> candies, String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(path), candies);
    }
}
